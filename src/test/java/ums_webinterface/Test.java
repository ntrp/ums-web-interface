package ums_webinterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.qantic.umsplugin.dto.config.ConfigurationParam;
import org.qantic.umsplugin.dto.config.ConfigurationParamWrapper;
import org.qantic.umsplugin.dto.config.exception.UnsupportedClassTypeException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Test {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, UnsupportedClassTypeException {

		ConfigurationParam configItem = new ConfigurationParam();

		configItem.setSetterType(ArrayList.class);

		String json = "";
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		json = objectMapper.writeValueAsString(configItem);
		System.out.println(json);
		ConfigurationParam param = objectMapper.readValue(json, ConfigurationParam.class);
		// System.out.println(param);
		ConfigurationParamWrapper lst = objectMapper.readValue(new File("./src/main/resources/WEB/ums_params_def.json"), ConfigurationParamWrapper.class);
		System.out.println(lst);
		System.out.println();

	}
}
