package org.qantic.umsplugin.services;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import net.pms.PMS;
import net.pms.configuration.RendererConfiguration;

import org.qantic.umsplugin.Const;
import org.qantic.umsplugin.dto.config.ConfigurationParam;
import org.qantic.umsplugin.dto.config.ConfigurationParamWrapper;
import org.qantic.umsplugin.dto.config.ProxyConfig;
import org.qantic.umsplugin.dto.feed.WebFeedWrapper;
import org.qantic.umsplugin.dto.files.DirectoryWrapper;
import org.qantic.umsplugin.dto.renderers.RendererItem;
import org.qantic.umsplugin.dto.renderers.RendererWrapper;
import org.qantic.umsplugin.managers.ConfigManager;
import org.qantic.umsplugin.managers.ProxyManager;
import org.qantic.umsplugin.managers.WebFeedManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnEvent;

public class Services {

	private static final Logger LOGGER = LoggerFactory.getLogger(Services.class);
	private static final String RENDERER_FIELD_NAME = "foundRenderers";

	private SocketIOServer interfaceServer;
	private ConfigManager configManager;
	private ProxyManager proxyManager;
	private WebFeedManager webFeedManager;

	public Services(SocketIOServer interfaceServer, ConfigManager configManager, ProxyManager proxyManager, WebFeedManager webFeedManager) {

		this.interfaceServer = interfaceServer;
		this.configManager = configManager;
		this.proxyManager = proxyManager;
		this.webFeedManager = webFeedManager;
	}

	@OnConnect
	public void onConnectHandler(SocketIOClient client) {
		onGetConfigtHandler(client, true, null);
		onGetProxyHandler(client, "", null);
		onGetFeedsHandler(client, "", null);
	}

	@SuppressWarnings("unchecked")
	@OnEvent("renderers:get")
	public void onGetPrototypeHandler(SocketIOClient client, String data, AckRequest ackRequest) {

		RendererWrapper renderList = new RendererWrapper();
		try {
			Field renderField = PMS.get().getClass().getDeclaredField(RENDERER_FIELD_NAME);
			renderField.setAccessible(true);
			ArrayList<RendererConfiguration> renderers = (ArrayList<RendererConfiguration>) renderField.get(PMS.get());
			for (RendererConfiguration config : renderers) {
				if (config != null) {
					renderList.getRendererList().add(new RendererItem(config.getRendererName(), config.getRendererIcon()));
				}
			}
		} catch (IllegalAccessException e) {
			LOGGER.error(Const.LOG_PREFIX + "illegal access exception", e);
		} catch (IllegalArgumentException e) {
			LOGGER.error(Const.LOG_PREFIX + "the passed argument is not valid", e);
		} catch (NoSuchFieldException e) {
			LOGGER.error(Const.LOG_PREFIX + "field [" + RENDERER_FIELD_NAME + "] not found");
		} catch (SecurityException e) {
			LOGGER.error(Const.LOG_PREFIX + "security exception", e);
		}
		client.sendEvent("renderers:set", renderList);
	}

	@OnEvent("children:get")
	public void onGetConfigtHandler(SocketIOClient client, String path, AckRequest ackRequest) {

		DirectoryWrapper wrapper = new DirectoryWrapper();

		if (path == null) {
			return;
		}

		path = path.startsWith("/") ? path : "/";
		path = path.endsWith("/") ? path : path + "/";

		File pathFile = new File(path);
		try {
			wrapper.setCurrentFolder(pathFile.getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		File[] files = pathFile.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() && !pathname.getName().startsWith(".");
			}
		});

		if (files != null) {
			Arrays.sort(files, new Comparator<File>() {

				@Override
				public int compare(File first, File second) {
					return first.getName().compareTo(second.getName());
				}
			});

			for (File child : files) {
				wrapper.getDirectories().add(child.getName());
			}
		}

		client.sendEvent("children:update", wrapper);
	}

	///   CONFIGURATION    ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@OnEvent("config:get")
	public void onGetConfigtHandler(SocketIOClient client, boolean force, AckRequest ackRequest) {

		if (force) {
			configManager.loadConfigDefinitions();
		}
		client.sendEvent("config:set", configManager.loadConfig());
	}

	@OnEvent("config:asyncset")
	public void onAsyncUpdateConfigtHandler(SocketIOClient client, ConfigurationParam param, AckRequest ackRequest) {

		configManager.saveConfigList(new ConfigurationParamWrapper(param));
	}

	@OnEvent("config:set")
	public void onSaveConfigtHandler(SocketIOClient client, ConfigurationParamWrapper paramList, AckRequest ackRequest) {

		configManager.saveConfigList(paramList);
		configManager.save();
		this.interfaceServer.getBroadcastOperations().sendEvent("config:set", paramList);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	///   PROXY   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@OnEvent("proxy:get")
	public void onGetProxyHandler(SocketIOClient client, String data, AckRequest ackRequest) {

		client.sendEvent("proxy:set", proxyManager.getProxy());
	}

	@OnEvent("proxy:set")
	public void onSaveProxyHandler(SocketIOClient client, ProxyConfig proxyConfig, AckRequest ackRequest) {

		proxyManager.saveAndSetProxy(proxyConfig);
		configManager.save();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	///   FEED   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@OnEvent("feeds:get")
	public void onGetFeedsHandler(SocketIOClient client, String data, AckRequest ackRequest) {

		client.sendEvent("feeds:set", webFeedManager.getFeeds());
	}

	@OnEvent("feeds:set")
	public void onSaveFeedsHandler(SocketIOClient client, WebFeedWrapper webFeedWrapper, AckRequest ackRequest) {

		webFeedManager.saveFeeds(webFeedWrapper);
		this.interfaceServer.getBroadcastOperations().sendEvent("feeds:set", webFeedWrapper);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@OnEvent("server:restart")
	public void onServerRestartHandler(SocketIOClient client, String data, AckRequest ackRequest) {

		PMS.get().reset();
	}

	@OnEvent("server:quit")
	public void onServerQuittHandler(SocketIOClient client, String data, AckRequest ackRequest) {

		System.exit(0);
	}
}
