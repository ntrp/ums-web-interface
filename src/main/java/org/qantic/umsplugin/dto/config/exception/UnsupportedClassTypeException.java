package org.qantic.umsplugin.dto.config.exception;

public class UnsupportedClassTypeException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnsupportedClassTypeException(String string) {
		super(string);
	}
}
