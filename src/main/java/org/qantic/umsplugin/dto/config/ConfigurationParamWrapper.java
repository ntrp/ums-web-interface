package org.qantic.umsplugin.dto.config;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigurationParamWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<ConfigurationParam> paramList;
	private ConfigurationParam sharesParam;

	public ConfigurationParamWrapper() {
		this(null);
	}

	public ConfigurationParamWrapper(ConfigurationParam param) {
		this.paramList = new ArrayList<ConfigurationParam>();
		if (param != null) {
			this.paramList.add(param);
		}
	}

	@JsonCreator
	public static ConfigurationParamWrapper deserialize(String json) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(json, ConfigurationParamWrapper.class);
	}

	@JsonProperty("@class")
	private String classRef() {
		return ConfigurationParamWrapper.class.getName();
	}

	public ArrayList<ConfigurationParam> getParamList() {
		return paramList;
	}

	public void setParamList(ArrayList<ConfigurationParam> paramList) {
		this.paramList = paramList;
	}

	public ConfigurationParam getSharesParam() {
		return sharesParam;
	}

	public void setSharesParam(ConfigurationParam sharesParam) {
		this.sharesParam = sharesParam;
	};
}
