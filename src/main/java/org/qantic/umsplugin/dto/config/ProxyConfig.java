package org.qantic.umsplugin.dto.config;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProxyConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean enable;
	private String host;
	private int port;
	private String user;
	private String pass;

	public ProxyConfig() {
		super();
	}

	public ProxyConfig(boolean enable, String host, int port, String user, String pass) {
		super();
		this.enable = enable;
		this.host = host;
		this.port = port;
		this.user = user;
		this.pass = pass;
	}

	@JsonProperty("@class")
	private String classRef() {
		return ProxyConfig.class.getName();
	};

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
}
