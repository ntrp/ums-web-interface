package org.qantic.umsplugin.dto.config;

import java.io.Serializable;

import org.qantic.umsplugin.dto.config.exception.UnsupportedClassTypeException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigurationParam implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("@class")
	private String classRef() {
		return ConfigurationParam.class.getName();
	};

	private String code;
	private String label;
	private String section;
	private String render;
	private String getter;
	private Class<?>[] getterTypes;
	private String setter;
	private Class<?>[] setterTypes;
	private Object options;
	private Object init;
	private Object value;

	public ConfigurationParam() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Class<?>[] getGetterTypes() {
		return getterTypes;
	}

	public void setGetterType(Class<?> type) throws UnsupportedClassTypeException {
		this.getterTypes = new Class<?>[] { type };
	}

	public void setGetterTypes(Class<?>[] types) throws UnsupportedClassTypeException {
		this.getterTypes = types;
	}

	public Class<?>[] getSetterTypes() {
		return setterTypes;
	}

	public void setSetterType(Class<?> type) throws UnsupportedClassTypeException {
		this.setterTypes = new Class<?>[] { type };
	}

	public void setSetterTypes(Class<?>[] types) throws UnsupportedClassTypeException {
		this.setterTypes = types;
	}

	public String getRender() {
		return render;
	}

	public void setRender(String render) {
		this.render = render;
	}

	public String getGetter() {
		return getter;
	}

	public void setGetter(String getter) {
		this.getter = getter;
	}

	public String getSetter() {
		return setter;
	}

	public void setSetter(String setter) {
		this.setter = setter;
	}

	public Object getOptions() {
		return options;
	}

	public void setOptions(Object options) {
		this.options = options;
	}

	public Object getInit() {
		return init;
	}

	public void setInit(Object init) {
		this.init = init;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
