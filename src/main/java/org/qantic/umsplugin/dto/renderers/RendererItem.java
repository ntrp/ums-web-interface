package org.qantic.umsplugin.dto.renderers;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RendererItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String icon;

	public RendererItem() {
		super();
	}

	public RendererItem(String name, String icon) {
		super();
		this.name = name;
		this.icon = icon;
	}

	@JsonProperty("@class")
	private String classRef() {
		return RendererItem.class.getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
