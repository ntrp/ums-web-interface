package org.qantic.umsplugin.dto.renderers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RendererWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<RendererItem> rendererList;

	public RendererWrapper() {
		this(null);
	}

	public RendererWrapper(RendererItem render) {
		this.rendererList = new ArrayList<RendererItem>();
		if (render != null) {
			this.rendererList.add(render);
		}
	}

	@JsonCreator
	public static RendererWrapper deserialize(String json) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(json, RendererWrapper.class);
	}

	@JsonProperty("@class")
	private String classRef() {
		return RendererWrapper.class.getName();
	}

	public ArrayList<RendererItem> getRendererList() {
		return rendererList;
	}

	public void setRendererList(ArrayList<RendererItem> rendererList) {
		this.rendererList = rendererList;
	}
}
