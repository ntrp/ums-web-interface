package org.qantic.umsplugin.dto.files;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DirectoryWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	private String currentFolder;
	private List<String> directories;

	@JsonCreator
	public static DirectoryWrapper deserialize(String json) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(json, DirectoryWrapper.class);
	}

	@JsonProperty("@class")
	private String classRef() {
		return DirectoryWrapper.class.getName();
	}

	public DirectoryWrapper() {
		super();
		this.directories = new ArrayList<String>();
	}

	public String getCurrentFolder() {
		return currentFolder;
	}

	public void setCurrentFolder(String currentFolder) {
		this.currentFolder = currentFolder;
	}

	public List<String> getDirectories() {
		return directories;
	}

	public void setDirectories(List<String> files) {
		this.directories = files;
	}
}
