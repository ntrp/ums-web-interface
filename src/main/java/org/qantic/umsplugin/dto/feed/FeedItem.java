package org.qantic.umsplugin.dto.feed;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String path;
	private String url;

	public FeedItem() {
		super();
	}

	public FeedItem(String path, String url) {
		super();
		this.path = path;
		this.url = url;
	}

	@JsonProperty("@class")
	private String classRef() {
		return FeedItem.class.getName();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String toFeedString(String type) {

		return type + "." + this.path + "=" + this.url;
	}
}
