package org.qantic.umsplugin.dto.feed;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "imagefeed", "audiofeed", "videofeed", "audiostream", "videostream" })
public class WebFeedWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<FeedItem> imagefeed;
	private List<FeedItem> audiofeed;
	private List<FeedItem> videofeed;
	private List<StreamItem> audiostream;
	private List<StreamItem> videostream;

	@JsonProperty("@feedPrototype")
	private final FeedItem feedPrototype;
	@JsonProperty("@streamPrototype")
	private final StreamItem streamPrototype;

	public WebFeedWrapper() {
		super();
		this.imagefeed = new ArrayList<FeedItem>();
		this.audiofeed = new ArrayList<FeedItem>();
		this.videofeed = new ArrayList<FeedItem>();
		this.audiostream = new ArrayList<StreamItem>();
		this.videostream = new ArrayList<StreamItem>();
		this.feedPrototype = new FeedItem("CHANGEME", "");
		this.streamPrototype = new StreamItem("CHANGEME", "", "", "");
	}

	@JsonCreator
	public static WebFeedWrapper deserialize(String json) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(json, WebFeedWrapper.class);
	}

	@JsonProperty("@class")
	private String classRef() {
		return WebFeedWrapper.class.getName();
	}

	public List<FeedItem> getImagefeed() {
		return imagefeed;
	}

	public void setImagefeed(List<FeedItem> imagefeed) {
		this.imagefeed = imagefeed;
	}

	public List<FeedItem> getAudiofeed() {
		return audiofeed;
	}

	public void setAudiofeed(List<FeedItem> audiofeed) {
		this.audiofeed = audiofeed;
	}

	public List<FeedItem> getVideofeed() {
		return videofeed;
	}

	public void setVideofeed(List<FeedItem> videofeed) {
		this.videofeed = videofeed;
	}

	public List<StreamItem> getAudiostream() {
		return audiostream;
	}

	public void setAudiostream(List<StreamItem> audiostream) {
		this.audiostream = audiostream;
	}

	public List<StreamItem> getVideostream() {
		return videostream;
	}

	public void setVideostream(List<StreamItem> videostream) {
		this.videostream = videostream;
	}

	public FeedItem getFeedPrototype() {
		return feedPrototype;
	}

	public StreamItem getStreamPrototype() {
		return streamPrototype;
	}
}
