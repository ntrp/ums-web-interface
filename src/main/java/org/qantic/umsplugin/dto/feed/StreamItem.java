package org.qantic.umsplugin.dto.feed;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StreamItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String path;
	private String name;
	private String url;
	private String thumb;

	public StreamItem() {
		super();
	}

	public StreamItem(String path, String name, String url, String thumb) {
		super();
		this.path = path;
		this.name = name;
		this.url = url;
		this.thumb = thumb;
	}

	@JsonProperty("@class")
	private String classRef() {
		return StreamItem.class.getName();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String toStreamString(String type) {

		return type + "." + this.path + "=" + this.name + "," + this.url + (StringUtils.isNotBlank(this.thumb) ? "," + this.thumb : "");
	}
}
