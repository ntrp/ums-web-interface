package org.qantic.umsplugin;

import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.buffer.CircularFifoBuffer;

import com.corundumstudio.socketio.BroadcastOperations;
import com.corundumstudio.socketio.SocketIOServer;

public class PrintStreamInterceptor extends PrintStream {

	Buffer stringBuffer;

	public PrintStreamInterceptor(OutputStream out, int maxLines) {

		super(out, true);
		this.stringBuffer = new CircularFifoBuffer(maxLines);
	}

	@Override
	public void write(int b) {
		super.write(b);
	}

	@Override
	public void write(byte[] buf, int off, int len) {

		String line = new String(buf, off, len).replaceAll("\\s*(\\w+\\s+)\\d+-\\d+-\\d+\\s(\\d\\d:\\d\\d:\\d\\d)[^\\]]+\\]\\s(.*)$", "$1$2  $3");
		SocketIOServer server = WebInterface.getServer();
		if (server != null) {
			BroadcastOperations bo = server.getBroadcastOperations();
			if (bo != null) {
				bo.sendEvent("log:update", line.trim());
			}
		}

		super.write(buf, off, len);
	}

	@Override
	public void print(String s) {
		super.print(s);
	}

	@Override
	public void println(String x) {
		super.println(x);
	}
}
