package org.qantic.umsplugin;

import javax.swing.JComponent;
import javax.swing.JPanel;

import net.pms.PMS;
import net.pms.dlna.DLNAMediaInfo;
import net.pms.dlna.DLNAResource;
import net.pms.external.StartStopListener;

import org.qantic.umsplugin.managers.ConfigManager;
import org.qantic.umsplugin.managers.ProxyManager;
import org.qantic.umsplugin.managers.WebFeedManager;
import org.qantic.umsplugin.services.Services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOChannelInitializer;
import com.corundumstudio.socketio.SocketIOServer;

public class WebInterface implements StartStopListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebInterface.class);

	private ConfigManager configManager;
	private ProxyManager proxyManager;
	private WebFeedManager webFeedManager;
	private static SocketIOServer interfaceServer;

	public WebInterface() {

		try {
			LOGGER.info(Const.LOG_PREFIX + "initializing WebInterface");
			// Intercepting logging stream and sending a copy to the client
			PrintStreamInterceptor systemOutBypass = new PrintStreamInterceptor(System.out, 10);
			System.setOut(systemOutBypass);
			// Initializing data structures
			configManager = new ConfigManager(PMS.getConfiguration());
			proxyManager = new ProxyManager(configManager);
			proxyManager.applyProxySettings();
			webFeedManager = new WebFeedManager(PMS.getConfiguration());
			// Starting interface server
			Configuration config = new Configuration();
			config.setHostname(configManager.getServerHost());
			config.setPort(configManager.getServerPort());
			config.setAllowCustomRequests(true);
			interfaceServer = new SocketIOServer(config);
			interfaceServer.addListeners(new Services(interfaceServer, configManager, proxyManager, webFeedManager));
			// configuring custom pipeline
			SocketIOChannelInitializer customInit = new ExtendedPipelineFactory(configManager);
			interfaceServer.setPipelineFactory(customInit);
			// starting the server
			interfaceServer.start();
			LOGGER.info(Const.LOG_PREFIX + "the interface is bound to '" + interfaceServer.getConfiguration().getHostname() + ":"
					+ interfaceServer.getConfiguration().getPort() + "' and is listening on '/web/gui'");
		} catch (Exception e) {
			LOGGER.error(Const.LOG_PREFIX + "error while setting up the web interface", e);
		}
	}

	public static SocketIOServer getServer() {

		return interfaceServer;
	}

	@Override
	public JComponent config() {

		return buildGui();
	}

	@Override
	public String name() {

		return "Web Interface";
	}

	@Override
	public void shutdown() {

		LOGGER.info(Const.LOG_PREFIX + "shutting down");
		interfaceServer.stop();
	}

	private JPanel buildGui() {

		return new JPanel();
	}

	@Override
	public void nowPlaying(DLNAMediaInfo media, DLNAResource resource) {
	}

	@Override
	public void donePlaying(DLNAMediaInfo media, DLNAResource resource) {
	}
}
