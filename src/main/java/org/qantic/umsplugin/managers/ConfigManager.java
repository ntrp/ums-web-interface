package org.qantic.umsplugin.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.pms.configuration.PmsConfiguration;
import net.pms.util.PropertiesUtil;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.qantic.umsplugin.Const;
import org.qantic.umsplugin.WebInterface;
import org.qantic.umsplugin.dto.config.ConfigurationParam;
import org.qantic.umsplugin.dto.config.ConfigurationParamWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class responsible of initializing the WebInterface configuration and of
 * offering an interface to the client for reading and saving configuration
 * items through JSON requests
 *
 */
public class ConfigManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigManager.class);

	public final boolean SERVER_UMS;
	public static final String DEF_SERVER_HOST = "0.0.0.0";
	public static final int DEF_SERVER_PORT = 8083;
	public static final String UMS_CONF_FILE_NAME = "UMS.conf";
	public static final String PMS_CONF_FILE_NAME = "PMS.conf";
	public static final String UMS_DEF_FILE_NAME = "ums_params_def.json";
	public static final String PMS_DEF_FILE_NAME = "pms_params_def.json";

	private final Map<String, String> defaultsMap = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("webinterface.host", "0.0.0.0");
			put("webinterface.port", "8083");
			put("webinterface.root", "WIROOT");
			put("webinterface.proxy.enable", "false");
			put("webinterface.proxy.host", "");
			put("webinterface.proxy.port", "-1");
			put("webinterface.proxy.user", "");
			put("webinterface.proxy.pass", "");
		}
	};
	private final PmsConfiguration pmsConfig;
	private final String configFileName;
	private final String paramsDefFileName;

	private ConfigurationParamWrapper paramList;

	public ConfigManager(PmsConfiguration pmsConf) {
		// Fixed properties
		SERVER_UMS = PropertiesUtil.getProjectProperties().get("project.name").equalsIgnoreCase("Universal Media Server");
		LOGGER.info(Const.LOG_PREFIX + PropertiesUtil.getProjectProperties().get("project.name") + " detected");
		this.pmsConfig = pmsConf;
		configFileName = SERVER_UMS ? UMS_CONF_FILE_NAME : PMS_CONF_FILE_NAME;
		paramsDefFileName = SERVER_UMS ? UMS_DEF_FILE_NAME : PMS_DEF_FILE_NAME;
		backupConfig();
		initCustomConfig();
		// init params based on folder location
		LOGGER.info("WebInterface: Parsing method mapping and loading the configuration data");
		// load configuration templates from the map
		loadConfigDefinitions();
	}

	private boolean backupConfig() {
		// backup config file
		String configFile = pmsConfig.getProfilePath();
		File bakFile = new File(configFile + ".bak");
		File confFile = new File(configFile);
		try {
			if (confFile.exists() && !bakFile.exists()) {
				FileUtils.copyFile(confFile, bakFile);
				LOGGER.info("WebInterface: Backing up your " + configFile + " as " + configFileName + ".bak");
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	/*
	 * Initialize the WebInterface configuration adding properties if missing
	 * and then saving it to file.
	 */
	private void initCustomConfig() {

		Object prop = null;
		boolean modified = false;

		for (Entry<String, String> confPair : defaultsMap.entrySet()) {
			prop = pmsConfig.getCustomProperty(confPair.getKey());
			if (prop == null) {
				pmsConfig.setCustomProperty(confPair.getKey(), confPair.getValue());
				modified = true;
			}
		}
		if (modified) {
			this.save();
		}
	}

	public void loadConfigDefinitions() {

		InputStream in = null;
		File confTmpl = new File(this.getWebInterfaceRoot() + "/" + paramsDefFileName);
		try {
			try {
				in = new FileInputStream(confTmpl);
				LOGGER.info(Const.LOG_PREFIX + "method mapping file retrieved from (External) -> " + confTmpl.getAbsolutePath());
			} catch (FileNotFoundException e) {
				LOGGER.info(Const.LOG_PREFIX + "method mapping file retrieved from (Jar) -> " + WebInterface.class.getResource("/WEB/" + paramsDefFileName));
				in = WebInterface.class.getResourceAsStream("/WEB/" + paramsDefFileName);
			}
			paramList = new ObjectMapper().readValue(in, ConfigurationParamWrapper.class);
		} catch (JsonParseException e) {
			LOGGER.error(Const.LOG_PREFIX + "error while mapping parameter definition json file", e);
		} catch (JsonMappingException e) {
			LOGGER.error(Const.LOG_PREFIX + "error while mapping parameter definition json file", e);
		} catch (IOException e) {
			LOGGER.error(Const.LOG_PREFIX + "error while mapping parameter definition json file", e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				LOGGER.error(Const.LOG_PREFIX + "error while closing input stream", e);
			}
		}
	}

	public ConfigurationParamWrapper loadConfig() {

		for (ConfigurationParam param : paramList.getParamList()) {
			this.loadParam(param);
		}
		this.loadParam(paramList.getSharesParam());
		return paramList;
	}

	private void loadParam(ConfigurationParam param) {

		Method getter = null;
		try {
			if (param.getGetterTypes() != null) {
				getter = PmsConfiguration.class.getMethod(param.getGetter(), param.getGetterTypes());
				param.setValue(getter.invoke(pmsConfig, new Object[] { null }));
			} else {
				getter = PmsConfiguration.class.getMethod(param.getGetter());
				param.setValue(getter.invoke(pmsConfig));
			}
		} catch (IllegalAccessException e) {
			LOGGER.error(Const.LOG_PREFIX + "illegal access exception", e);
		} catch (IllegalArgumentException e) {
			LOGGER.error(Const.LOG_PREFIX + "the passed argument is not valid", e);
		} catch (InvocationTargetException e) {
			LOGGER.error(Const.LOG_PREFIX + "invokation target exception", e);
		} catch (NoSuchMethodException e) {
			LOGGER.error(Const.LOG_PREFIX + "setter method [" + param.getGetter() + "] not found. The associated parameter will not be saved");
		} catch (SecurityException e) {
			LOGGER.error(Const.LOG_PREFIX + "security exception", e);
		}
	}

	public void saveConfigList(ConfigurationParamWrapper paramList) {

		for (ConfigurationParam param : paramList.getParamList()) {
			this.saveParam(param);
		}
		this.saveParam(paramList.getSharesParam());
	}

	private void saveParam(ConfigurationParam param) {

		Method setter = null;
		try {
			setter = PmsConfiguration.class.getMethod(param.getSetter(), param.getSetterTypes());
			Object value = param.getValue();
			if (value instanceof List<?>) {
				value = StringUtils.join((List<?>) value, ",");
			}
			setter.invoke(pmsConfig, value);
		} catch (IllegalAccessException e) {
			LOGGER.error(Const.LOG_PREFIX + "illegal access exception", e);
		} catch (IllegalArgumentException e) {
			LOGGER.error(Const.LOG_PREFIX + "the passed argument is not valid", e);
		} catch (InvocationTargetException e) {
			LOGGER.error(Const.LOG_PREFIX + "invokation target exception.", e);
		} catch (NoSuchMethodException e) {
			LOGGER.error(Const.LOG_PREFIX + "setter method [" + param.getSetter() + "] not found. The associated parameter will not be saved");
		} catch (SecurityException e) {
			LOGGER.error(Const.LOG_PREFIX + "security exception", e);
		}
	}

	/**
	 * Save UMS configuration to file
	 */
	public void save() {

		try {
			pmsConfig.save();
		} catch (ConfigurationException e) {
			LOGGER.error(Const.LOG_PREFIX + "unable to save UMS configuration file", e);
		}
	}

	public PmsConfiguration getConfig() {

		return pmsConfig;
	}

	/*
	 * Getters and Setters
	 */
	public String getServerHost() {
		return this.getStrValue(pmsConfig.getCustomProperty("webinterface.host"), DEF_SERVER_HOST);
	}

	public void setServerHost(String host) {
		pmsConfig.setCustomProperty("webinterface.host", host);
	}

	public int getServerPort() {
		return this.getIntValue(pmsConfig.getCustomProperty("webinterface.port"), DEF_SERVER_PORT);
	}

	public void setServerPort(int port) {
		pmsConfig.setCustomProperty("webinterface.port", String.valueOf(port));
	}

	public String getWebInterfaceRoot() {
		return (String) pmsConfig.getCustomProperty("webinterface.root");
	}

	public void setWebIterfaceRoot(String root) {
		pmsConfig.setCustomProperty("webinterface.root", root);
	}

	public boolean isProxyEnabled() {
		return this.getBooleanValue(pmsConfig.getCustomProperty("webinterface.proxy.enable"), false);
	}

	public void setProxyEnabled(boolean enable) {
		pmsConfig.setCustomProperty("webinterface.proxy.enable", String.valueOf(enable));
	}

	public String getProxyHost() {
		return (String) pmsConfig.getCustomProperty("webinterface.proxy.host");
	}

	public void setProxyHost(String host) {
		pmsConfig.setCustomProperty("webinterface.proxy.host", host);
	}

	public int getProxyPort() {
		return this.getIntValue(pmsConfig.getCustomProperty("webinterface.proxy.port"), -1);
	}

	public void setProxyPort(int port) {
		pmsConfig.setCustomProperty("webinterface.proxy.port", String.valueOf(port));
	}

	public String getProxyUser() {
		return (String) pmsConfig.getCustomProperty("webinterface.proxy.user");
	}

	public void setProxyUser(String user) {
		pmsConfig.setCustomProperty("webinterface.proxy.user", user);
	}

	public String getProxyPass() {
		return (String) pmsConfig.getCustomProperty("webinterface.proxy.pass");
	}

	public void setProxyPass(String pass) {
		pmsConfig.setCustomProperty("webinterface.proxy.pass", pass);
	}

	private String getStrValue(Object str, String def) {

		return (String) (str != null ? str : def);
	}

	private int getIntValue(Object str, int def) {

		int port = -1;
		try {
			port = Integer.parseInt((String) str);
		} catch (NumberFormatException e) {
			port = def;
			LOGGER.error(Const.LOG_PREFIX + "not a valid integer: " + str + ". using default value [" + def + "]");
		}
		return port;
	}

	private boolean getBooleanValue(Object str, boolean def) {

		boolean val = def;
		try {
			Boolean.valueOf((String) str);
		} catch (Exception e) {
			LOGGER.error(Const.LOG_PREFIX + "not a valid boolean: " + str + ". using default value [" + def + "]");
		}
		return val;
	}
}
