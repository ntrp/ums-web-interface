package org.qantic.umsplugin.managers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import net.pms.configuration.PmsConfiguration;

import org.apache.commons.io.FileUtils;
import org.qantic.umsplugin.Const;
import org.qantic.umsplugin.dto.feed.FeedItem;
import org.qantic.umsplugin.dto.feed.StreamItem;
import org.qantic.umsplugin.dto.feed.WebFeedWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ntrp
 *
 */
public class WebFeedManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebFeedManager.class);

	private final String feedsFileName;
	private WebFeedWrapper feeds = new WebFeedWrapper();

	public WebFeedManager(PmsConfiguration pmsConf) {

		this.feedsFileName = pmsConf.getWebConfPath();
		this.backupDefaultFeeds();
		this.loadFeedsFromFile();
	}

	private void backupDefaultFeeds() {
		// Creating backup file
		File bakFile = new File(feedsFileName + ".bak");
		File feedFile = new File(feedsFileName);
		try { // if not already backed up
			if (feedFile.exists() && !bakFile.exists()) {
				FileUtils.copyFile(feedFile, bakFile);
				LOGGER.info(Const.LOG_PREFIX + "backing up your " + feedsFileName + " as " + feedsFileName + ".bak");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public WebFeedWrapper getFeeds() {
		return this.feeds;
	}

	/**
	 * Load the feeds from file to the feeds object
	 *
	 */
	public void loadFeedsFromFile() {

		if (feedsFileName != null) {
			try {
				// create a reader for the feed file
				BufferedReader input = new BufferedReader(new FileReader(new File(feedsFileName)));
				try {
					String line = null;
					LOGGER.info(Const.LOG_PREFIX + "parsing web feed file " + feedsFileName);
					while ((line = input.readLine()) != null) {
						int first = line.indexOf('.');
						int second = line.indexOf('=');
						if (!(first >= 0) || !(second >= 0)) {
							continue;
						}
						String path = line.substring(first + 1, second);
						String rest = line.substring(second + 1, line.length());
						if (line.startsWith("imagefeed")) {
							feeds.getImagefeed().add(new FeedItem(path, rest));
						} else if (line.startsWith("audiofeed")) {
							feeds.getAudiofeed().add(new FeedItem(path, rest));
						} else if (line.startsWith("videofeed")) {
							feeds.getVideofeed().add(new FeedItem(path, rest));
						} else if (line.startsWith("audiostream")) {
							String[] rests = rest.split(",");
							feeds.getAudiostream().add(new StreamItem(path, rests[0], rests[1], rests.length > 2 ? rests[2] : ""));
						} else if (line.startsWith("videostream")) {
							String[] rests = rest.split(",");
							feeds.getVideostream().add(new StreamItem(path, rests[0], rests[1], rests.length > 2 ? rests[2] : ""));
						}
					}
				} finally {
					input.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Save the feed list to file
	 */
	public void saveFeeds(WebFeedWrapper webFeedWrapper) {

		feeds = webFeedWrapper;
		StringBuilder feedList = new StringBuilder();
		feedList.append("#########################################\n");
		feedList.append("#### UMS WebInterface generated file ####\n");
		feedList.append("#########################################\n");
		feedList.append("\n#### imagefeed\n");
		for (FeedItem feed : feeds.getImagefeed()) {
			feedList.append(feed.toFeedString("imagefeed") + "\n");
		}
		feedList.append("\n#### audiofeed\n");
		for (FeedItem feed : feeds.getAudiofeed()) {
			feedList.append(feed.toFeedString("audiofeed") + "\n");
		}
		feedList.append("\n#### videofeed\n");
		for (FeedItem feed : feeds.getVideofeed()) {
			feedList.append(feed.toFeedString("videofeed") + "\n");
		}
		feedList.append("\n#### audiostream\n");
		for (StreamItem stream : feeds.getAudiostream()) {
			feedList.append(stream.toStreamString("audiostream") + "\n");
		}
		feedList.append("\n#### videostream\n");
		for (StreamItem stream : feeds.getVideostream()) {
			feedList.append(stream.toStreamString("videostream") + "\n");
		}
		// safe the feeds to file
		File file = new File(feedsFileName);
		try {
			FileOutputStream fos = new FileOutputStream(file, false);
			fos.write(feedList.toString().getBytes());
			fos.close();
			LOGGER.info(Const.LOG_PREFIX + "web feed configuration saved to " + feedsFileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
