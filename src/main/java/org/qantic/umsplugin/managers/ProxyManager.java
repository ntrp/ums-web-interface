package org.qantic.umsplugin.managers;

import org.qantic.umsplugin.Const;
import org.qantic.umsplugin.dto.config.ProxyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProxyManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProxyManager.class);
	private final ConfigManager configManager;

	public ProxyManager(final ConfigManager cm) {

		configManager = cm;
	}

	public void saveAndSetProxy(ProxyConfig proxyConfig) {

		boolean enabled = System.getProperty("proxySet", "").equals("true");

		configManager.setProxyHost(proxyConfig.getHost());
		configManager.setProxyPort(proxyConfig.getPort());
		configManager.setProxyUser(proxyConfig.getUser());
		configManager.setProxyPass(proxyConfig.getPass());
		configManager.setProxyEnabled(proxyConfig.isEnable());

		this.applyProxySettings();

		if (proxyConfig.isEnable()) {
			LOGGER.info(Const.LOG_PREFIX + "proxy set to " + proxyConfig.getHost() + ":" + proxyConfig.getPort());
		} else {
			if (enabled) {
				LOGGER.info(Const.LOG_PREFIX + "proxy disabled");
			}
		}
	}

	public ProxyConfig getProxy() {

		return new ProxyConfig(configManager.isProxyEnabled(), configManager.getProxyHost(), configManager.getProxyPort(), configManager.getProxyUser(),
				configManager.getProxyPass());
	}

	public void applyProxySettings() {

		System.getProperties().put("http.proxyHost", configManager.getProxyHost());
		System.getProperties().put("http.proxyPort", configManager.getProxyPort());
		System.getProperties().put("http.proxyUser", configManager.getProxyUser());
		System.getProperties().put("http.proxyPassword", configManager.getProxyPass());
		System.getProperties().put("proxySet", configManager.isProxyEnabled());
	}
}
