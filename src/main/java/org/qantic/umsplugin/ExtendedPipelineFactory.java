package org.qantic.umsplugin;

import io.netty.channel.Channel;

import org.qantic.umsplugin.managers.ConfigManager;

import com.corundumstudio.socketio.SocketIOChannelInitializer;

public class ExtendedPipelineFactory extends SocketIOChannelInitializer {

	private final HttpStaticFileServerHandler staticFileServerHandler;

	public ExtendedPipelineFactory(ConfigManager configManager) {

		this.staticFileServerHandler = new HttpStaticFileServerHandler(configManager);
	}

	@Override
	protected void initChannel(Channel ch) throws Exception {

		super.initChannel(ch);
		ch.pipeline().addBefore(SocketIOChannelInitializer.PACKET_HANDLER, "HttpStaticHandler", staticFileServerHandler);
	}
}
