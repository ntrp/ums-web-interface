angular.module('wiApp').directive('configParam', function() {
    return {
        replace: true,
        restrict: 'AE',  
        templateUrl: "config_render.tmpl"
    };
});

angular.module('wiApp').directive('autoScroll', [function () {
	return {
		restrict: 'A',
		scope: {
			autoScroll: "="
		},
		link: function postLink(scope, elem, attrs) {

			var el = elem[0];

			scope.$watch(
				function () { 
					return scope.autoScroll;
				}, 
				function (newVal, oldVal) {
					if (newVal !== oldVal) {
						el.scrollTop = el.scrollHeight;
					}
				}
			);
		}
	};
}]);