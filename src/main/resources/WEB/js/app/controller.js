angular.module('wiApp').controller('RootCtrl', ['$scope', '$interval', '$modal', 'socket', function ($scope, $interval, $modal, socket) {

	$scope.connected = false;
	$scope.console = "";
	$scope.scrollBack = 100;
	$scope.proxyPassVisible = false;
	$scope.configs = {};
	$scope.proxy = {};
	$scope.feeds = {};

	socket.on('connect', function () {
		$scope.connected = true;
		socket.emit('renderers:get','');
		$scope.renderTimedPoll = $interval(function () {
			socket.emit('renderers:get','');
		}, 5000);
	});

	socket.on('disconnect', function () {
		$scope.connected = false;
		$interval.cancel($scope.renderTimedPoll);
	});

	// EVENT callbacks
	socket.on('log:update', function (data) {
		$scope.console += data + '\n';
		if ($scope.console.match(/\n/g).length > $scope.scrollBack) {
			$scope.console = $scope.console.substring($scope.console.indexOf('\n') + 1, $scope.console.lenght);
		}
	});
	
	socket.on('config:set', function (data) {
		$scope.configs = angular.fromJson(data);
		$scope.configs.sharesParam.splitValue = $scope.configs.sharesParam.value.split(','); 
	});
	
	socket.on('proxy:set', function (data) {
		$scope.proxy = angular.fromJson(data);
	});
	
	socket.on('feeds:set', function (data) {
		$scope.feeds = angular.fromJson(data);
	});

	socket.on('renderers:set', function (data) {
		$scope.renderers = angular.fromJson(data);
	});
	
	// Actions
	$scope.saveConfig = function () {
		socket.emit('config:set', $scope.configs);
		socket.emit('feeds:set', $scope.feeds);
	};

	$scope.restartServer = function () {
		socket.emit('server:restart', '');
	};

	$scope.quitServer = function () {
		socket.emit('server:quit', '');
	};

	$scope.saveProxy = function () {
		socket.emit('proxy:set', $scope.proxy);
	};
	
	// proxy
	$scope.passVisible = function (visible) {
		$scope.proxyPassVisible = visible; 
	};

	// select feed categoy
	$scope.setCurrentFeedCat = function (feedCat) {
		$scope.currentFeedCat = feedCat;
		delete $scope.currentFeed;
	};
	// select feed
	$scope.setCurrentFeed = function (feed) {
		$scope.currentFeed = feed;
	};
	// remove selected
	$scope.removeFeed = function (feedCat, feed) {
		$scope.feeds[feedCat].splice($scope.feeds[feedCat].indexOf(feed), 1);
	};
	// add empty in selected category
	$scope.addEmptyFeed = function (feedCat) {
		var emptyFeed = feedCat.indexOf('stream') > -1 ? angular.copy($scope.feeds['@streamPrototype']) : angular.copy($scope.feeds['@feedPrototype'])
		$scope.feeds[feedCat].push(emptyFeed);
	};
	
}]);

angular.module('wiApp').controller('SingleFolderController', ['$scope', '$interval', '$modal', 'socket', function ($scope, $interval, $modal, socket) {
	// folder management
	$scope.add = function (scope, key) {

		var modalInstance = $modal.open({
			templateUrl: 'folder_select_render.tmpl',
			controller: 'FolderSelectModalController'
		});

		modalInstance.result.then(function (value) {
			scope[key] = value;
		}, function () {
		});
	};

	$scope.clear = function (scope, key) {
		scope[key] = '';	
	}
}]);

angular.module('wiApp').controller('MultiFolderController', ['$scope', '$interval', '$modal', 'socket', function ($scope, $interval, $modal, socket) {
	
	$scope.add = function () {

		var modalInstance = $modal.open({
			templateUrl: 'folder_select_render.tmpl',
			controller: 'FolderSelectModalController'
		});

		modalInstance.result.then(function (value) {
			$scope.configs.sharesParam.splitValue.push(value);
			$scope.configs.sharesParam.value = $scope.configs.sharesParam.splitValue.join(',');
		}, function () {
		});
	};
	
	$scope.remove = function (value) {
		$scope.configs.sharesParam.splitValue.splice($scope.configs.sharesParam.splitValue.indexOf(value), 1);
		$scope.configs.sharesParam.value = $scope.configs.sharesParam.splitValue.join(',');
	};
}]);

angular.module('wiApp').controller('FolderSelectModalController', ['$scope', '$modalInstance', 'socket', function ($scope, $modalInstance, socket) {

	socket.emit('children:get', '/');
	
	socket.on('children:update', function (data) {
		$scope.browser = angular.fromJson(data);
	});

	$scope.ok = function () {
    	$modalInstance.close($scope.browser.currentFolder);
  	};

  	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	$scope.chageDir = function (dir) {
		socket.emit('children:get', $scope.browser.currentFolder + '/' + dir);
		return false;
	};
}]);