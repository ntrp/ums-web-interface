# UMS Web Interface #

UMS Web Interface is a feature rich and customizable remote configuration interface for the popular DLNA UniversalMediaServer. It is based on the following technologies:

* [AngularJS](https://angularjs.org/) and [Socket.io](http://socket.io/) client frontend.
* [Netty-Socket.io](https://github.com/mrniko/netty-socketio) server backend.

Included features at the moment:

* Found renderers list.
* Web feeds and streams management.
* UMS proxy settings management.
* Real-time log.
* Various configurations like shared folders and many others.

The default url for the interface is http://localhost:8083/web/gui, the port and the host can be changed in UMS.conf (webinterface.port, webinterface.host options).

Some screenshots of the interface can be found [Here](https://bitbucket.org/ntrp/ums-web-interface/wiki/Screenshots).